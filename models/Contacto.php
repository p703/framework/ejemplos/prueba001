<?php
namespace app\models;

use yii\base\Model;
use Yii;

Class Contacto extends Model{
    public $nombre;
    public $email;
    public $telefono;
    public $direccion;
    public $asunto;
    public $fecha;
    public $politicas;
    
    public function attributeLabels(){
        return [
            "nombre" => "Nombre completo",
            "email" => "Correo electrónico",
            "telefono" => "Teléfono",
            "direccion" => "Dirección completa",
            "asunto" => "¿Cuál es tu pregunta?",
            "fecha" => "Fecha Nacimiento",
            "politicas" => "Aceptar política de privacidad"
        ];
    }
    
    public function rules(){
        return[
            [["nombre","email","telefono"],"required"],
            ["email","email"],//campo, tipo de dato
            ["nombre","string","length"=>[1,100]],
            ["politicas",'boolean'],
            //["fecha","date","format"=>'yyyy/mm/dd'],
            ["fecha","string"],
            [["direccion","asunto"],"safe"],
            ["politicas","comprobarpoliticas"],//chequear si has aceptado las politicas
        ];
    }
    /**
     * chequear que hayas aceptado las politicas
     * 
     */
    public function comprobarpoliticas($attribute,$params){
        if($this->$attribute==0){
            $this->addError($attribute,"Es necesario que aceptes las políticas de privacidad");
        }
    }
    
    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['contacto'])
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                ->setReplyTo([$this->email => $this->nombre])
                ->setSubject("contacto")
                ->setTextBody($this->asunto)
                ->send();

            return true;
        }
        return false;
    }

}

