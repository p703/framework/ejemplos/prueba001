<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Productos;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionOfertas() {
       //listView
        $dataProvider=new ActiveDataProvider([
            'query'=>Productos::find()->where(['oferta'=>1]),
        ]);
        
        return $this->render("ofertas",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionProductos() {
        //GridView
        $dataProvider=new ActiveDataProvider([
            'query'=> Productos::find()
        ]);
        return $this->render("productos",[
            'datos'=>$dataProvider,
        ]);
    }
    
    public function actionCategorias() {
        //listView
        $dataProvider=new ActiveDataProvider([
            'query'=>Productos::find()->select("categorias")->distinct()
        ]);
        
        return $this->render("categorias",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionVer($categoria) {
        //listView
        $productos =Productos::find()->where(['categorias'=>$categoria]);
        
        $dataProvider=new ActiveDataProvider([
            'query'=>$productos,
        ]);
                
        return $this->render("ver",[
            'dataProvider'=>$dataProvider,
            'categoria'=>$categoria,
        ]);
    }
    public function actionDonde() {
        return $this->render('donde');
    }
    
    public function actionQuienes() {
        return $this->render('quienes');
    }
    
    public function actionNuestros() {
        return $this->render('nuestros');
    }
    
    public function actionInfo() {
        return $this->render('info');
    }
    
    public function actionContacto(){
    $model = new \app\models\Contacto();//objeto de tipo contacto
    
    if ($model->load(Yii::$app->request->post())) {
        if ($model->contact()) {
            //Hay frameworks que no dejan poner lo de las políticas
            //en el modelo. En ese caso se haría aquí: 
            // $model->addError("politicas","No has aceptado las politicas");
            return $this->render("mostrar");
        }
    }

    return $this->render('contacto', [
        'model' => $model,
    ]);
    }
    
   

}