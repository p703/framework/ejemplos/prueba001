<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $datos,
    'columns'=>[
        'id',
        'nombre',
        [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url='@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=>'width:200px'
                ]);
            }
        ],
        'descripcion',
        'precio',
        'oferta',
        ],
    ]);


