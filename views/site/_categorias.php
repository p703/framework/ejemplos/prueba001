<?php
use yii\helpers\Html;
?>

  <ul class="list-group list-group-flush">
    <li class="list-group-item">Categoría: <?= $model->categorias ?></li>
    
    <li class="list-group-item">
        <?= Html::a('Ver productos', 
                ['site/ver', 'categoria' => $model->categorias],
                ['class' => 'btn btn-primary']) ?></li>
  </ul>
