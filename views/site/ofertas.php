<?php
use yii\widgets\ListView;
?>
<h1 class="list-group-item">Productos en oferta:</h1>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_ofertas',
    'itemOptions' => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    'options' => [
        'class' => 'row',
    ],
    'layout'=>"{items}"
]);
